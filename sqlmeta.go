package sqlmeta

type Table struct {
	Name string
}

func NewTable(name string) *Table {
	return &Table{Name: name}
}
